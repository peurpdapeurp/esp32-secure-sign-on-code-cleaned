#ifndef CONNECTIVITY_HPP
#define CONNECTIVITY_HPP

#include "consts.hpp"

#include <esp8266ndn.h>
#include <WiFi.h>

/** \brief Establish and maintain NDN connectivity over WiFi.
 */
class ConnectivityClass
{
public:
  ConnectivityClass();

  void
  begin();

  void
  loop();

  IPAddress
  localIP();

  ndn::Face&
  getFace()
  {
    return m_face;
  }

private:
  ndn::UdpTransport m_transport;
  ndn::Face m_face;

};

extern ConnectivityClass Connectivity;

#endif // CONNECTIVITY_HPP
