/*
#ifndef CONSTS_HPP
#define CONSTS_HPP

#include <cstddef>
#include <stdint.h>
#include <uECC.h>

#define PIN_BTN 0

#define IP_CLIENT IPAddress(192, 168, 137, 215)
#define IP_SERVER IPAddress(192, 168, 137, 243)
#define IP_SUBNET IPAddress(255, 255, 255, 0)
#define UDP_PORT 6363

#ifdef DEBUG
#undef DEBUG
#endif
#define DEBUG 1

// maximum number of name components that we will process
constexpr int MAX_NAME_COMPS = 15;

constexpr uint8_t DATA_TLV_TYPE = 0x06;
constexpr uint8_t NAME_TLV_TYPE = 0x07;
constexpr uint8_t METAINFO_TLV_TYPE = 0x14;
constexpr uint8_t CONTENT_TLV_TYPE = 0x15;

// #define SECURITY_ECC_256 0
 #define SECURITY_ECC_HYBRID 0
// #define SECURITY_ECC_160 0
// #define SECURITY_RSA_3072 0
// #define SECURITY_RSA_HYBRID 0
// #define SECURITY_RSA_1024 0

#if defined(SECURITY_ECC_256)
constexpr size_t ECDH_AND_KD_PRIVATE_KEY_SIZE = 32;
constexpr size_t ECDH_AND_KD_PUBLIC_KEY_SIZE = 64;
#endif

#if defined(SECURITY_ECC_160) || defined(SECURITY_ECC_HYBRID)
constexpr size_t ECDH_AND_KD_PRIVATE_KEY_SIZE = 21;
constexpr size_t ECDH_AND_KD_PUBLIC_KEY_SIZE = 40;
#endif

#if defined(SECURITY_RSA_1024)
// this is a 1024 RSA diffie hellman pem file
const unsigned char DH_PARAMS_PEM[] =
    "-----BEGIN DH PARAMETERS-----\n"
    "MIGHAoGBAP//////////yQ/aoiFowjTExmKLgNwc0SkCTgiKZ8x0Agu+pjsTmyJR\n"
    "Sgh5jjQE3e+VGbPNOkMbMCsKbfJfFDdP4TVtbVHCReSFtXZiXn7G9ExC6aY37WsL\n"
    "/1y29Aa37e44a/taiZ+lrp8kEXxLH+ZJKGZR7OZTgf//////////AgEC\n"
    "-----END DH PARAMETERS-----";
constexpr int DH_PRIVATE_EXPONENT_SIZE_BYTES = 128;
constexpr int DH_ARRAY_SIZES = 500;
// RSA 1024 pem key file
const unsigned char RSA_KEY_PAIR_PEM_FILE[] =
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIICXQIBAAKBgQDUjoGa38NDDg7/XJ9Mb0mC7HFOFibtOsgcIY48f51qXU82rvV0\n"
    "t0Pvyzs0GOvhQoGcGM/jK5T4Knf22GELiI+nDR1IqSQE/IBxN1E9WssZfnG3nnm0\n"
    "mDa7Gps1vhAilHRKzzUGmIV4WNOVKLT39mfcE3y+95kjYwd0w8cKqV5KVwIDAQAB\n"
    "AoGBAIuVDZ1DxjlZmRWuoFmUV0TnujxaJbPxvwliK8Kk1x3LKsKxQDUmSDHHVxiO\n"
    "KRBjGTQFwzbeHtSdJuxuCcSGKDaLouA1APRD04fRzS8Sy8NvWZOBXWm5G63DnJyo\n"
    "L2X7YkYn2F6rqHzzg7Tk68ONtnUxKyUcBseTQcQCRb/CeuTxAkEA8/nYoRk8JrUV\n"
    "Qdd7aAJzspy7fnNWN94nUm6a7kVxKsPkhoOfDRewN1XM2TXiW+pykqqB0VSkunjw\n"
    "VqFqsbcbKQJBAN8IQScsyKbsJI3FjvWFYEt3URYFLCm+V75JKs4aI3HD8tlj0Rqw\n"
    "PUEUR50i9xFSBniUq+zKA6VavbiIOArEaX8CQQChaORXAHj5KshbfRIZ57Vfr62j\n"
    "pO6fm4vsT1yU0sBiCKPCEoZBhTCCC3OxKKMPKHaQbdnhAdzRuX1X8wbBeGCxAkBv\n"
    "SVIhNwJj7/6JUyNzNPsIjN/D6g7QQW7MvvuB3Z8D/C8n89t5wqi92V2mdbTsSo56\n"
    "Ck0DGQXasz1pX2b/UGQHAkBXc/XW6dSm/eb1XMnw++vKcSx5LFa7uJ4S3WPv5C4H\n"
    "1FGylDOXI3WzX4C+e58IlBM8GLFv2j2kXX0BAKwKZL6P\n"
    "-----END RSA PRIVATE KEY-----";

#endif

#if defined(SECURITY_RSA_HYBRID)
// this is a 1024 RSA diffie hellman pem file
const unsigned char DH_PARAMS_PEM[] =
    "-----BEGIN DH PARAMETERS-----\n"
    "MIGHAoGBAP//////////yQ/aoiFowjTExmKLgNwc0SkCTgiKZ8x0Agu+pjsTmyJR\n"
    "Sgh5jjQE3e+VGbPNOkMbMCsKbfJfFDdP4TVtbVHCReSFtXZiXn7G9ExC6aY37WsL\n"
    "/1y29Aa37e44a/taiZ+lrp8kEXxLH+ZJKGZR7OZTgf//////////AgEC\n"
    "-----END DH PARAMETERS-----";
constexpr int DH_PRIVATE_EXPONENT_SIZE_BYTES = 128;
constexpr int DH_ARRAY_SIZES = 500;
// RSA 3072 pem key file
const unsigned char RSA_KEY_PAIR_PEM_FILE[] =
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIIG4wIBAAKCAYEAmKBziaRy+blvsrjL/FgEI9g/+md8+rvF3b5JRPsxLabh3RhH\n"
    "bt+JwV3ulzxyVAsOJWMbP2a/Y903gY0ZDXKdiOOnYCuwuexgN1evX5pgr5hXsEmO\n"
    "JsrwfiPuxTOAq8CSqmkOEELLpSRLfDIdjjzRFkAmVC88hJPTw+iUaNod4Vijf9Su\n"
    "xpg59tfZcvXW99+vKpTNks5Lb9iWs4wziTrrN46QNQM7Wf5nW8rGId4X3ZPY2L+Z\n"
    "KO+xQKzmrlkT5VFKoIDmNWwL3dHd/LHKy/HN3BtF18CXJOWZuCEsQHOy3y7Mdec9\n"
    "JMJzQxT1Mh/Tvc1ZUsun8HJ3uhsKol/GwHjUKuZDn2n91PFUtLK0wJ6wqr7Q9dys\n"
    "xxN/BXBrGkO28WkJbJitIzN/A081ypIJ8X9HyU7tL5B0wxXXv53m7C4+iGYg9XMB\n"
    "Xk1ovu9z0FJyiN6B/ludhHN97uGseAWqTf9zvXOvcrhJ6pdXEc8T4bB5BaAVYvxY\n"
    "/qzR7bK3pwyWv8gRAgMBAAECggGADMreBDphXKb7QrqUdlkWN77nVeCYrBdS9Uv+\n"
    "riZzr8TeB38CdbuMu0VZOxnY50xp6h5NaVn+Tqt6/IPMBsGFjLSwEbJ3xI2YXusd\n"
    "iuDQm8ckCmWP4ZZ+48O1Ppgbf8nLQTc+84P1t6i8HvPAYGyehcPosoOnCeGuDSyQ\n"
    "S92ERZ/9TDiSUZjuus7KrgUG+I2qC+Ke/GAIGJ7JutDUfPsQPf7X/WmA7wEQvrPX\n"
    "OX14+Jxsv3YHq1Ozwpz7VL+omD7g5kFCl0MMoAQggiJ6gvHNSCaQqCSwMac4Vv7Q\n"
    "q3fRUpiyU5bIwN3LIJ2XglkyuBlVnEYM3oJKyzRSKHCzYkrLVwo7B/gzbXUuonmE\n"
    "5WMDZuLzUWz0wyo4PrM8WOBGabHjBPwmIeFVm1FIBuZw9gGCPJDHQxfNIkVeQU5k\n"
    "Gee9vu195Wq5zR0FQb4a3T/Lb6tu0sQJmdcTiiVB3zmGjfMpiVB3iBBsZ42y7cs1\n"
    "oU+lL66FaISXg/MuwfHg1ISE10nFAoHBAMjOaYZaasU9zRAKHH53kZ65ggI8UZuD\n"
    "dSbglLawNMkZg0Jd9OJVcdFcN6rL/rnYWt4n7wjo51ueiYPkc9cSDZZuz7UzmL1t\n"
    "37ujYRTfBmDaVVufAIyE9nLKAFk+xtMWkS+Li0lL0jZJRGUgDavWpvJ8UxPlskFi\n"
    "f92REf5T4TET5yIhgBwyJa30NZ0RDrxC4x98FZD4ddqOIRya1VmCiZ6SjNj+4/jB\n"
    "OWx+EUDPxIr4WmWaW2tsBd0GI2C+ojbdUwKBwQDCk+vntd6oaYnCJu2qWJ9qvEfJ\n"
    "JnyzZs27j+3KVctuThnKe+QHZg/TdwWVFuqgUEJ4YjxlpHDXU8FCJBaDJG16ttT7\n"
    "uZ2Lz4L4ZnXiGBWT1n3qS41e6Mmu9p8h7Z9CtN2cHf9oriRbu3PVpdEAhZ3mS3TI\n"
    "nUfK/IjtKJbWdvkQJQcH0B6yVs49PTUAqJZj8I7gOy+APfYsPnpqx0JoUGJipIJx\n"
    "FkJ6waJCjfMVs/zjP/vojOl2mvHDL/fxm30kdIsCgcEAikHsUBADAYkanpeHpN+o\n"
    "iJOLWwSy35M0Xy8z1VEfFvMU9+X/DLLt6ctHnLZVzRmU/u6Z2xWY7fELF598zrDk\n"
    "YBYttsIuwQZcjDHiTt974cWSDcI9O4L0uHYypT/e30njbsvye+XTd4Mcr0/ReeYS\n"
    "DlThUorNH8204hXXQLf6DbN5aXuNXFx+qvXA0EMFVUPsYyopIyae5nsDe1Fz4exJ\n"
    "2r9mcKWWyxgm9HccY/ZpiZGTk+8iUKUQqGxogVzgDUx3AoHAbxKHsN61+sc8N1OH\n"
    "ih1K62ANTnsk5jhCKtaPdFgjZ5U3zcmJMtwFHr6rth690sPeHeBd/9ut+o06HeqB\n"
    "21zZDhr+W6/qilvrtKawll8POjRP7oYwbkROlQz9bB28MxUSCENrgDMgWCkah5Q/\n"
    "SZ/8sfDS+o4l5G7iLxdje4uww5GPgNtCGqXI11ZwQi76opQ4yzGd/QPh2gHUWc3j\n"
    "T67Launj+HAvhOmyGXH0mKgFLShiQa7muqjaZ4Sjcg1Dk6LfAoHAKGH1Mowf5zp9\n"
    "hGDv+mR9Ab6Up2gqGBz80z5ATgnR8S2u7LtaitVX5Uql0NIhZ4JzRiEJgpN6T92y\n"
    "jLuYGqjnHe0/jTPhKf7HbPFzgA9cDs4cfAxU+3Xd3zcNHWP6usYG5CXex0+IBcIs\n"
    "3NsFx4IDFeAxw+s2y0JlVw5svjXH0XSJXvdDIkR7KOi2LFfuNJKLsXQ3UzhIftIk\n"
    "qQ2comnYZhwVTidX9QwH9PbO790uW1HlxcqCYlwFQeyDa74KU/oB\n"
    "-----END RSA PRIVATE KEY-----";

#endif

#if defined(SECURITY_RSA_3072)
const unsigned char DH_PARAMS_PEM[] =
    "-----BEGIN DH PARAMETERS-----\n"
    "MIIBiAKCAYEA///////////JD9qiIWjCNMTGYouA3BzRKQJOCIpnzHQCC76mOxOb\n"
    "IlFKCHmONATd75UZs806QxswKwpt8l8UN0/hNW1tUcJF5IW1dmJefsb0TELppjft\n"
    "awv/XLb0Brft7jhr+1qJn6WunyQRfEsf5kkoZlHs5Fs9wgB8uKFjvwWY2kg2HFXT\n"
    "mmkWP6j9JM9fg2VdI9yjrZYcYvNWIIVSu57VKQdwlpZtZww1Tkq8mATxdGwIyhgh\n"
    "fDKQXkYuNs474553LBgOhgObJ4Oi7Aeij7XFXfBvTFLJ3ivL9pVYFxg5lUl86pVq\n"
    "5RXSJhiY+gUQFXKOWoqqxC2tMxcNBFB6M6hVIavfHLpk7PuFBFjb7wqK6nFXXQYM\n"
    "fbOXD4Wm4eTHq/WujNsJM9cejJTgSiVhnc7j0iYa0u5r8S/6BtmKCGTYdgJzPshq\n"
    "ZFIfKxgXeyAMu+EXV3phXWx3CYjAutlG4gjiT6B05asxQ9tb/OD9EI5LgtEgqTrS\n"
    "yv//////////AgEC\n"
    "-----END DH PARAMETERS-----";
constexpr int DH_PRIVATE_EXPONENT_SIZE_BYTES = 128;
constexpr int DH_ARRAY_SIZES = 500;
// RSA 3072 pem key file
const unsigned char RSA_KEY_PAIR_PEM_FILE[] =
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIIG4wIBAAKCAYEAmKBziaRy+blvsrjL/FgEI9g/+md8+rvF3b5JRPsxLabh3RhH\n"
    "bt+JwV3ulzxyVAsOJWMbP2a/Y903gY0ZDXKdiOOnYCuwuexgN1evX5pgr5hXsEmO\n"
    "JsrwfiPuxTOAq8CSqmkOEELLpSRLfDIdjjzRFkAmVC88hJPTw+iUaNod4Vijf9Su\n"
    "xpg59tfZcvXW99+vKpTNks5Lb9iWs4wziTrrN46QNQM7Wf5nW8rGId4X3ZPY2L+Z\n"
    "KO+xQKzmrlkT5VFKoIDmNWwL3dHd/LHKy/HN3BtF18CXJOWZuCEsQHOy3y7Mdec9\n"
    "JMJzQxT1Mh/Tvc1ZUsun8HJ3uhsKol/GwHjUKuZDn2n91PFUtLK0wJ6wqr7Q9dys\n"
    "xxN/BXBrGkO28WkJbJitIzN/A081ypIJ8X9HyU7tL5B0wxXXv53m7C4+iGYg9XMB\n"
    "Xk1ovu9z0FJyiN6B/ludhHN97uGseAWqTf9zvXOvcrhJ6pdXEc8T4bB5BaAVYvxY\n"
    "/qzR7bK3pwyWv8gRAgMBAAECggGADMreBDphXKb7QrqUdlkWN77nVeCYrBdS9Uv+\n"
    "riZzr8TeB38CdbuMu0VZOxnY50xp6h5NaVn+Tqt6/IPMBsGFjLSwEbJ3xI2YXusd\n"
    "iuDQm8ckCmWP4ZZ+48O1Ppgbf8nLQTc+84P1t6i8HvPAYGyehcPosoOnCeGuDSyQ\n"
    "S92ERZ/9TDiSUZjuus7KrgUG+I2qC+Ke/GAIGJ7JutDUfPsQPf7X/WmA7wEQvrPX\n"
    "OX14+Jxsv3YHq1Ozwpz7VL+omD7g5kFCl0MMoAQggiJ6gvHNSCaQqCSwMac4Vv7Q\n"
    "q3fRUpiyU5bIwN3LIJ2XglkyuBlVnEYM3oJKyzRSKHCzYkrLVwo7B/gzbXUuonmE\n"
    "5WMDZuLzUWz0wyo4PrM8WOBGabHjBPwmIeFVm1FIBuZw9gGCPJDHQxfNIkVeQU5k\n"
    "Gee9vu195Wq5zR0FQb4a3T/Lb6tu0sQJmdcTiiVB3zmGjfMpiVB3iBBsZ42y7cs1\n"
    "oU+lL66FaISXg/MuwfHg1ISE10nFAoHBAMjOaYZaasU9zRAKHH53kZ65ggI8UZuD\n"
    "dSbglLawNMkZg0Jd9OJVcdFcN6rL/rnYWt4n7wjo51ueiYPkc9cSDZZuz7UzmL1t\n"
    "37ujYRTfBmDaVVufAIyE9nLKAFk+xtMWkS+Li0lL0jZJRGUgDavWpvJ8UxPlskFi\n"
    "f92REf5T4TET5yIhgBwyJa30NZ0RDrxC4x98FZD4ddqOIRya1VmCiZ6SjNj+4/jB\n"
    "OWx+EUDPxIr4WmWaW2tsBd0GI2C+ojbdUwKBwQDCk+vntd6oaYnCJu2qWJ9qvEfJ\n"
    "JnyzZs27j+3KVctuThnKe+QHZg/TdwWVFuqgUEJ4YjxlpHDXU8FCJBaDJG16ttT7\n"
    "uZ2Lz4L4ZnXiGBWT1n3qS41e6Mmu9p8h7Z9CtN2cHf9oriRbu3PVpdEAhZ3mS3TI\n"
    "nUfK/IjtKJbWdvkQJQcH0B6yVs49PTUAqJZj8I7gOy+APfYsPnpqx0JoUGJipIJx\n"
    "FkJ6waJCjfMVs/zjP/vojOl2mvHDL/fxm30kdIsCgcEAikHsUBADAYkanpeHpN+o\n"
    "iJOLWwSy35M0Xy8z1VEfFvMU9+X/DLLt6ctHnLZVzRmU/u6Z2xWY7fELF598zrDk\n"
    "YBYttsIuwQZcjDHiTt974cWSDcI9O4L0uHYypT/e30njbsvye+XTd4Mcr0/ReeYS\n"
    "DlThUorNH8204hXXQLf6DbN5aXuNXFx+qvXA0EMFVUPsYyopIyae5nsDe1Fz4exJ\n"
    "2r9mcKWWyxgm9HccY/ZpiZGTk+8iUKUQqGxogVzgDUx3AoHAbxKHsN61+sc8N1OH\n"
    "ih1K62ANTnsk5jhCKtaPdFgjZ5U3zcmJMtwFHr6rth690sPeHeBd/9ut+o06HeqB\n"
    "21zZDhr+W6/qilvrtKawll8POjRP7oYwbkROlQz9bB28MxUSCENrgDMgWCkah5Q/\n"
    "SZ/8sfDS+o4l5G7iLxdje4uww5GPgNtCGqXI11ZwQi76opQ4yzGd/QPh2gHUWc3j\n"
    "T67Launj+HAvhOmyGXH0mKgFLShiQa7muqjaZ4Sjcg1Dk6LfAoHAKGH1Mowf5zp9\n"
    "hGDv+mR9Ab6Up2gqGBz80z5ATgnR8S2u7LtaitVX5Uql0NIhZ4JzRiEJgpN6T92y\n"
    "jLuYGqjnHe0/jTPhKf7HbPFzgA9cDs4cfAxU+3Xd3zcNHWP6usYG5CXex0+IBcIs\n"
    "3NsFx4IDFeAxw+s2y0JlVw5svjXH0XSJXvdDIkR7KOi2LFfuNJKLsXQ3UzhIftIk\n"
    "qQ2comnYZhwVTidX9QwH9PbO790uW1HlxcqCYlwFQeyDa74KU/oB\n"
    "-----END RSA PRIVATE KEY-----";
#endif

#endif // CONSTS_HPP
*/