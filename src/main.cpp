#include "connectivity.hpp"
#include "credentials.hpp"
#include "consts.hpp"
#include "utils/logger.hpp"

#include "secure-sign-on-variant-selector.hpp"

#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/ECC_VARIANT_TYPES/160/sign-on-basic-ecc-160.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/ECC_VARIANT_TYPES/HYBRID/sign-on-basic-ecc-hybrid.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/ECC_VARIANT_TYPES/256/sign-on-basic-ecc-256.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/RSA_VARIANT_TYPES/1024/sign-on-basic-rsa-1024.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/RSA_VARIANT_TYPES/HYBRID/sign-on-basic-rsa-hybrid.hpp"

#include "hardcoded-experimentation.hpp"

#define LOG(...) LOGGER(Main, __VA_ARGS__)

#define PIN_BTN 0

#if defined(SIGN_ON_BASIC_ECC_160)
SignOnBasicECC160 m_secure_sign_on_client(Connectivity.getFace(), DEVICE_IDENTIFIER, DEVICE_CAPABILITIES, 
  SECURE_SIGN_ON_CODE, BOOTSTRAP_ECC_PUBLIC, BOOTSTRAP_ECC_PRIVATE);
#endif
#if defined (SIGN_ON_BASIC_ECC_HYBRID)
SignOnBasicECCHybrid m_secure_sign_on_client(Connectivity.getFace(), DEVICE_IDENTIFIER, DEVICE_CAPABILITIES,
  SECURE_SIGN_ON_CODE, BOOTSTRAP_ECC_PUBLIC, BOOTSTRAP_ECC_PRIVATE);
#endif
#if defined (SIGN_ON_BASIC_ECC_256)
SignOnBasicECC256 m_secure_sign_on_client(Connectivity.getFace(), DEVICE_IDENTIFIER, DEVICE_CAPABILITIES,
  SECURE_SIGN_ON_CODE, BOOTSTRAP_ECC_PUBLIC, BOOTSTRAP_ECC_PRIVATE);
#endif
#if defined (SIGN_ON_BASIC_RSA_1024)
SignOnBasicRSA1024 m_secure_sign_on_client(Connectivity.getFace(), 
  DEVICE_IDENTIFIER, DEVICE_CAPABILITIES,
  SECURE_SIGN_ON_CODE, DH_PARAMS_PEM, sizeof(DH_PARAMS_PEM), 
  RSA_KEY_PAIR_PEM_FILE, sizeof(RSA_KEY_PAIR_PEM_FILE));
#endif
#if defined (SIGN_ON_BASIC_RSA_HYBRID)
SignOnBasicRSAHybrid m_secure_sign_on_client(Connectivity.getFace(), 
  DEVICE_IDENTIFIER, DEVICE_CAPABILITIES,
  SECURE_SIGN_ON_CODE, DH_PARAMS_PEM, sizeof(DH_PARAMS_PEM), 
  RSA_KEY_PAIR_PEM_FILE, sizeof(RSA_KEY_PAIR_PEM_FILE));
#endif


bool m_isBtnDown, m_wasBtnDown;
bool m_firstOnboarding = true;
bool m_firstGotNetworkPrefix = true;

void
setup()
{
  Serial.begin(9600);
  Serial.println();
  ndn::setLogOutput(Serial);

  Connectivity.begin();

  LOG("Made it past initializing connectivity.");

  m_secure_sign_on_client.begin();

  LOG("Made it past initializing secure sign-on client.");
}

void
loop()
{
  Connectivity.loop();
  delay(1);

  m_wasBtnDown = false;
  bool isBtnDown = !digitalRead(PIN_BTN);
  if (!m_wasBtnDown && isBtnDown) {
    if (!m_secure_sign_on_client.onboardingCompleted()) {
      m_secure_sign_on_client.initiateSignOn();
      delay(1000);
    }
  }
  m_wasBtnDown = m_isBtnDown;

  if (m_secure_sign_on_client.onboardingCompleted() && m_firstOnboarding) {

    m_firstOnboarding = false;

    ndn_NameComponent tempComps[MAX_NAME_COMPS];
    ndn::NameLite tempName(tempComps, MAX_NAME_COMPS);
    
    for (int i = 0; i < m_secure_sign_on_client.getNetworkPrefix().size(); i++)
      tempName.append(m_secure_sign_on_client.getNetworkPrefix().get(i));

    LOG("Network prefix: " << ndn::PrintUri(m_secure_sign_on_client.getNetworkPrefix()) << endl);

    m_secure_sign_on_client.printLastExecutionTimes();

  }
}