#ifndef SECURE_SIGN_ON_VARIANT_SELECTOR_HPP
#define SECURE_SIGN_ON_VARIANT_SELECTOR_HPP

// this file is currently included in the following files
// (meaning that there are compile time variations in these files depending on the
//  secure sign-on variant uncommented below):
    // hardcoded-experimentation.hpp
        // this changes the hardcoded credentials used for experimentation purposes
    // main.cpp
        // this changes the secure sign-on variant type used for m_secure_sign_on_client

// uncomment the security type and then recompile and reupload to the device
// *** MAKE SURE ONLY ONE IS UNCOMMENTED AT A TIME
// #define SIGN_ON_BASIC_ECC_160
// #define SIGN_ON_BASIC_ECC_HYBRID
 #define SIGN_ON_BASIC_ECC_256
// #define SIGN_ON_BASIC_RSA_1024
// #define SIGN_ON_BASIC_RSA_HYBRID
// #define SIGN_ON_BASIC_RSA_3072

#endif // SECURE_SIGN_ON_VARIANT_SELECTOR_HPP