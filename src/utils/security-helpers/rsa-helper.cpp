
#include "rsa-helper.hpp"
#include "../logger.hpp"
#include "../print-byte-array-helper.hpp"
#include "general-security-helper.hpp"

#define LOG(...) LOGGER(RSAHelper, __VA_ARGS__)

RSAHelper::RSAHelper(const unsigned char *rsa_pem, size_t rsa_pem_length)
{
  int ret;
  if ((ret = mbedtls_ctr_drbg_seed( &m_ctr_drbg, mbedtls_entropy_func, &m_entropy,
        NULL,
        0)) != 0 )
    LOG("Failed to seed the deterministic random number generator.");
  else
    LOG("Succeeded in seeding deterministic random number generator.");

  mbedtls_pk_init(&m_pk);
  int signResult = mbedtls_pk_parse_key(&m_pk, rsa_pem, rsa_pem_length, nullptr, 0);
  if (signResult != 0)
    LOG("Parsing RSA private key file failed: " << signResult << endl);
}

RSAHelper::~RSAHelper()
{

}

int
RSAHelper::generateApplicationLevelInterestAsymmetricSignatureRSA(const ndn::InterestLite &interest, 
  uint8_t *signatureOutput, size_t *signatureLength)
{
  uint8_t encoding[2000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeName
      (interest.getName(), &signedPortionBeginOffset, &signedPortionEndOffset, output, &encodingLength))) {
      LOG("Error decoding interest name for signing.");
      LOG("Error: " << ndn_getErrorString(error) << endl);
      return 1;
  }
  
  uint8_t interestNameDigest[ndn_SHA256_DIGEST_SIZE];
  ndn_digestSha256(encoding, encodingLength, interestNameDigest);

  LOG("Digest of what we are signing with RSA: " << endl << 
  PrintByteArray(interestNameDigest, 0, ndn_SHA256_DIGEST_SIZE) << endl);

  int ret;
  if ((ret = mbedtls_pk_sign(&m_pk, MBEDTLS_MD_SHA256, interestNameDigest, ndn_SHA256_DIGEST_SIZE, 
    signatureOutput, signatureLength, NULL, NULL)) != 0) {
    LOG("Generation of application level interest signature with RSA failed.");
    return 1;
  }

  LOG("Successfully generated RSA signature." << endl);
  LOG("Value of signatureLength after generating RSA signature: " << PriUint64<DEC>(*signatureLength) << endl);

  return 0;
}