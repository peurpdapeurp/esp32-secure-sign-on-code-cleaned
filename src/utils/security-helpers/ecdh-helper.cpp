
#include "ecdh-helper.hpp"
#include "../logger.hpp"
#include "../print-byte-array-helper.hpp"
#include "general-security-helper.hpp"

#define LOG(...) LOGGER(ECDHHelper, __VA_ARGS__)

ECDHHelper::ECDHHelper()
{
  uECC_set_rng(GeneralSecurityHelper::RNG);
}

ECDHHelper::~ECDHHelper()
{

}

// this is implemented using uECC
int
ECDHHelper::generateEcKeyPair(uint8_t *pub, size_t *pub_length, uint8_t *pri, 
  size_t *pri_length, uECC_Curve curve)
{
  *pri_length = uECC_curve_private_key_size(curve);
  *pub_length = uECC_curve_public_key_size(curve);
  int ret;
  if ((ret = uECC_make_key(pub, pri, curve)) == 0)
    return 1;
  return 0;
}

// this is implemented using uECC
int
ECDHHelper::deriveSharedSecretECDH(const uint8_t *pub, const uint8_t *pri,
                       uint8_t *sharedSecretBytes, uECC_Curve curve)
{
  int ret;
  if ((ret = uECC_shared_secret(pub, pri, sharedSecretBytes, curve)) == 0)
    return 1;
  return 0;
}