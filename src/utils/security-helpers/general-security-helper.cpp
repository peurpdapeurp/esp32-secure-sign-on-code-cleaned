
#include "general-security-helper.hpp"
#include <esp8266ndn.h>
#include "../logger.hpp"
#include "../print-byte-array-helper.hpp"
#include <Streaming.h>

#include "mbedtls/md.h"
 #include <mbedtls/sha256.h>

#include <AES.h>

constexpr int MAX_NAME_COMPS = 15;

#define LOG(...) LOGGER(GeneralSecurityHelper, __VA_ARGS__)

GeneralSecurityHelper::GeneralSecurityHelper()
{

}

GeneralSecurityHelper::~GeneralSecurityHelper()
{
    
}

bool
GeneralSecurityHelper::verifyDataBySymmetricKey(const ndn::DataLite &data, 
  const uint8_t *keyBytes, size_t keyLength)
{
  ndn_NameComponent dataNameComps[MAX_NAME_COMPS];
  ndn_NameComponent dataKeyLocatorNameComps[MAX_NAME_COMPS];
  ndn::DataLite tempData(dataNameComps, MAX_NAME_COMPS, dataKeyLocatorNameComps, MAX_NAME_COMPS);

  tempData.set(data);

  ndn::HmacKey key(keyBytes, keyLength);

  uint8_t encoding[2000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t encodingLength, signedPortionBeginOffset, signedPortionEndOffset;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeData
      (tempData, &signedPortionBeginOffset,
      &signedPortionEndOffset, output, &encodingLength))) {
    LOG("Error encoding data from bootstrapping response, in order to do verification by TSK.");
    LOG("Error: " << ndn_getErrorString(error) << endl);
    return false;
  }

  if (!key.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
                 tempData.getSignature().getSignature().buf(), tempData.getSignature().getSignature().size())) {
    LOG("Failed to verify data by symmetric key." << endl);
    return false;
  }

  return true;
}

void 
GeneralSecurityHelper::decrypt(const uint8_t * cipherText, size_t cipherTextLength, uint8_t *key, 
  const size_t keyLength, uint8_t *outputBuffer) {

  AES aes;

  uint8_t cipherTextCopy[cipherTextLength];
  memcpy(cipherTextCopy, cipherText, cipherTextLength);

  LOG("Cipher text copy: " << endl << PrintByteArray(cipherTextCopy, 0, cipherTextLength));

  aes.iv_inc();
  
  byte iv[N_BLOCK];
  unsigned long long myIv = 0;
  for (int i = 0; i < N_BLOCK; i++) {
    iv[i] = 0;
  }
 
  aes.set_IV(myIv);
  aes.get_IV(iv);

  LOG("cipherTextLength: " << PriUint64<DEC>(cipherTextLength) << endl);
  size_t keyLengthTimesEight = keyLength * 8;
  LOG("keyLength*8: " << PriUint64<DEC>(keyLengthTimesEight) << endl);
  
  aes.do_aes_decrypt(cipherTextCopy, cipherTextLength, outputBuffer, key, keyLength*8, iv);

  LOG("Cipher text copy after decrypting: " << endl << 
    PrintByteArray(cipherTextCopy, 0, cipherTextLength) << endl);

}

int 
GeneralSecurityHelper::RNG(uint8_t *dest, unsigned size) {
  // Use the least-significant bits from the ADC for an unconnected pin (or connected to a source of 
  // random noise). This can take a long time to generate random data if the result of analogRead(0) 
  // doesn't change very frequently.
  while (size) {
    uint8_t val = 0;
    for (unsigned i = 0; i < 8; ++i) {
      int init = esp_random();
      int count = 0;
      while (esp_random() == init) {
        ++count;
      }
      
      if (count == 0) {
         val = (val << 1) | (init & 0x01);
      } else {
         val = (val << 1) | (count & 0x01);
      }
    }
    *dest = val;
    ++dest;
    --size;
  }
  // NOTE: it would be a good idea to hash the resulting random data using SHA-256 or similar.
  return 1;
}

bool
GeneralSecurityHelper::checkBuffersEqual(const uint8_t *buf1, size_t buf1Length, const uint8_t *buf2, size_t buf2Length) {

  if (buf1Length != buf2Length)
    return false;

  for (int i = 0; i < buf1Length; i++) {
    if (buf1[i] != buf2[i])
      return false;
  }

  return true;
}