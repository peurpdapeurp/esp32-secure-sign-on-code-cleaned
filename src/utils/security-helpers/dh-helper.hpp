#ifndef DH_HELPER_HPP
#define DH_HELPER_HPP

#include <stdint.h>
#include <stddef.h>

#include "mbedtls/config.h"
#include "mbedtls/platform.h"
#include "mbedtls/entropy.h"  // entropy gathering
#include "mbedtls/ctr_drbg.h" // CSPRNG
#include "mbedtls/error.h"    // error code to string conversion
#include "mbedtls/md.h"       // hash algorithms
#include "mbedtls/sha256.h"
#include "mbedtls/dhm.h"

/** \brief Wrapper object for mbedtls ecdh interface.
 */
class DHHelper
{
public:
  DHHelper(const unsigned char *dh_params, size_t dh_params_length);

  ~DHHelper();

  // this is implemented using mbedtls
  // returns 1 on success, 0 on failure
  int
  generateDHKeyPair(uint8_t *tokenOutput, size_t *tokenSize, int privateExponentNumBytes);

  // this is implemented using mbedtls
  // returns 1 on success, 0 on failure
  int readDHPublicAndDeriveSharedSecret(const uint8_t *otherPubTokenBytes,
    size_t otherPubTokenBytesLength,
    uint8_t *sharedSecretOutput, size_t sharedSecretOutputLength, size_t *sharedSecretLength);

  mbedtls_entropy_context m_entropy;
  mbedtls_ctr_drbg_context m_ctr_drbg;
  mbedtls_dhm_context m_dhm;
  mbedtls_md_context_t m_ctx;
  
};

#endif // DH_HELPER_HPP