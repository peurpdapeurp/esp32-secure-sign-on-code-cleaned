#ifndef ECDSA_HELPER_HPP
#define ECDSA_HELPER_HPP

#include <stdint.h>
#include <stddef.h>

#include <esp8266ndn.h>

#include <uECC.h>

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class ECDSAHelper
{
public:
  explicit ECDSAHelper();

  ~ECDSAHelper();

  // this is implemented using uECC
  // returns 0 on success, 1 on failure
  int
  generateApplicationLevelInterestAsymmetricSignatureECC(const ndn::InterestLite &interest, 
    const uint8_t *key, size_t keyLength, uint8_t *signatureOutput, 
    size_t *signatureLength, uECC_Curve curve);

private:

  /** \brief Determine ASN1 length of integer at integer[0:20].
 */
  int
  determineAsn1IntLength(const uint8_t *integer, uECC_Curve curve)
  {

    int priKeySize = uECC_curve_private_key_size(curve);

    // if the top bit of the first integer is 1, then an extra 0 byte needs to be added 
    // to keep the integer positive since it is encoded in two's complement
    if ((integer[0] & 0x80) != 0x00)
    {
      return priKeySize + 1;
    }

    // if the top bit of the first integer is 0, then still need to check if there are more
    // zero bytes that can be cut off, since the ASN.1 encoding of a r and s ECDSA signature pair
    // requires that r and s be represented a minimal amount of bytes possible
    int len = priKeySize;
    for (int i = 0; i < priKeySize - 1; ++i)
    {
      if (((integer[i] << 8) | (integer[i + 1] & 0x80)) != 0x0000)
      {
        break;
      }
      --len;
    }
    return len;
  }

  uint8_t *
  writeAsn1Int(uint8_t *output, const uint8_t *integer, int length, uECC_Curve curve)
  {
    *(output++) = ASN1_INTEGER;
    *(output++) = static_cast<uint8_t>(length);

    int priKeySize = uECC_curve_private_key_size(curve);

    if (length == priKeySize + 1)
    {
      *(output++) = 0x00;
      memmove(output, integer, priKeySize);
      return output + priKeySize;
    }

    memmove(output, integer + priKeySize - length, length);
    return output + length;
  }

  /** \brief Encode x-octet raw signature at sig[8:x+8] as DER at sig[0:retval].
    */
  void
  encodeSignatureBits(uint8_t *sig, size_t *sigLength, uECC_Curve curve)
  {
    const uint8_t *begin = sig;
    const uint8_t *r = sig + 8;
    const uint8_t *s = r + uECC_curve_private_key_size(curve);
    int rLength = determineAsn1IntLength(r, curve);
    int sLength = determineAsn1IntLength(s, curve);

    *(sig++) = ASN1_SEQUENCE;
    *(sig++) = 2 + rLength + 2 + sLength;
    sig = writeAsn1Int(sig, r, rLength, curve);
    sig = writeAsn1Int(sig, s, sLength, curve);

    *sigLength = sig - begin;
  }

  uECC_Curve m_curve;

  enum
  {
    ASN1_SEQUENCE = 0x30,
    ASN1_INTEGER = 0x02,
  };
};

#endif // ECDSA_HELPER_HPP