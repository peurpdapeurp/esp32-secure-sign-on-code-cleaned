
#include "ecdsa-helper.hpp"
#include "../logger.hpp"
#include "../print-byte-array-helper.hpp"
#include "general-security-helper.hpp"

#define LOG(...) LOGGER(ECDSAHelper, __VA_ARGS__)

ECDSAHelper::ECDSAHelper()
{
  uECC_set_rng(GeneralSecurityHelper::RNG);
}

ECDSAHelper::~ECDSAHelper()
{

}

int
ECDSAHelper::generateApplicationLevelInterestAsymmetricSignatureECC(const ndn::InterestLite& interest,  
  const uint8_t *key, size_t keyLength, uint8_t *signatureOutput, size_t *signatureLength,
  uECC_Curve curve) {

  // will just do interest signing without a signature info for now

  int privateKeySize = uECC_curve_private_key_size(curve);

  uint8_t encoding[1000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeName
      (interest.getName(), &signedPortionBeginOffset, &signedPortionEndOffset, output, &encodingLength))) {
      LOG("Error decoding interest name for signing.");
      LOG("Error: " << ndn_getErrorString(error) << endl);
      return 1;
  }

  uint8_t interestNameDigest[ndn_SHA256_DIGEST_SIZE];
  ndn_digestSha256(encoding, encodingLength, interestNameDigest);

  int ret;
  *signatureLength = (privateKeySize * 2);
  if ((ret = (uECC_sign(key, interestNameDigest, ndn_SHA256_DIGEST_SIZE, signatureOutput + 8, curve))) == 0)
    return 1;

  LOG("Bytes of signed portion hash: " << endl << PrintByteArray(interestNameDigest, 0, ndn_SHA256_DIGEST_SIZE) << endl);

  LOG("Bytes of raw signature generated for interest: " << endl << PrintByteArray(signatureOutput + 8, 0, *signatureLength) << endl);

  encodeSignatureBits(signatureOutput, signatureLength, curve);

  return 0;

}