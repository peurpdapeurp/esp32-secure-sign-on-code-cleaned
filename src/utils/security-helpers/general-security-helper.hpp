#ifndef GENERAL_SECURITY_HELPER_HPP
#define GENERAL_SECURITY_HELPER_HPP

#include <stdint.h>
#include <stddef.h>

#include <esp8266ndn.h>

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class GeneralSecurityHelper
{
public:
  explicit GeneralSecurityHelper();

  ~GeneralSecurityHelper();

  static bool
  verifyDataBySymmetricKey(const ndn::DataLite &data, const uint8_t *keyBytes, size_t keyLength);
  
  static void
  decrypt(const uint8_t *cipherText, size_t cipherTextLength, uint8_t *key, 
    const size_t keyLength, uint8_t *outputBuffer);

  static bool
  checkBuffersEqual(const uint8_t *buf1, size_t buf1Length, const uint8_t *buf2, size_t buf2Length);

  static int
  RNG(uint8_t *dest, unsigned size);

};

#endif // GENERAL_SECURITY_HELPER_HPP