#ifndef RSA_HELPER_HPP
#define RSA_HELPER_HPP

#include <stdint.h>
#include <stddef.h>

#include <esp8266ndn.h>

#include "mbedtls/config.h"
#include "mbedtls/platform.h"
#include "mbedtls/entropy.h"  // entropy gathering
#include "mbedtls/ctr_drbg.h" // CSPRNG
#include "mbedtls/error.h"    // error code to string conversion
#include "mbedtls/md.h"       // hash algorithms
#include "mbedtls/sha256.h"
#include "mbedtls/pk.h"

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class RSAHelper
{
public:
  explicit RSAHelper(const unsigned char *rsa_pem, size_t rsa_pem_length);

  ~RSAHelper();

  // this is implemented using uECC
  // returns 0 on success, 1 on failure
  int
  generateApplicationLevelInterestAsymmetricSignatureRSA(const ndn::InterestLite &interest, 
  uint8_t *signatureOutput, size_t *signatureLength);

private:

  mbedtls_entropy_context m_entropy;
  mbedtls_ctr_drbg_context m_ctr_drbg;
  mbedtls_md_context_t m_ctx;

  mbedtls_pk_context m_pk;

};

#endif // RSA_HELPER_HPP