
#include "dh-helper.hpp"
#include "../logger.hpp"
#include "../print-byte-array-helper.hpp"
#include "general-security-helper.hpp"

#define LOG(...) LOGGER(DHHelper, __VA_ARGS__)

DHHelper::DHHelper(const unsigned char *dh_params, size_t dh_params_length)
{
  int ret;
  if ((ret = mbedtls_ctr_drbg_seed( &m_ctr_drbg, mbedtls_entropy_func, &m_entropy,
        NULL,
        0)) != 0 )
    LOG("Failed to seed the deterministic random number generator.");
  else
    LOG("Succeeded in seeding deterministic random number generator.");

  if ((ret = mbedtls_dhm_parse_dhm(&m_dhm, dh_params, dh_params_length) != 0))
    LOG("Failed to read DH parameters.");
  else
    LOG("Succeeded in reading DH parameters.");
}

DHHelper::~DHHelper()
{

}

// this is implemented using mbedtls
int
DHHelper::generateDHKeyPair(uint8_t *tokenOutput, size_t *tokenSize, int privateExponentNumBytes)
{
  int ret;
  if ((ret = mbedtls_dhm_make_public(&m_dhm, privateExponentNumBytes, tokenOutput, m_dhm.len, 
        mbedtls_ctr_drbg_random, &m_ctr_drbg))) {
    LOG("Failed to make public token.");
    return 1;
  }
  else
    LOG("Bytes of public token: " << endl << PrintByteArray(tokenOutput, 0, m_dhm.len) << endl);

  *tokenSize = m_dhm.len;
  return 0;
}

// this is implemented using mbedtls
int
DHHelper::readDHPublicAndDeriveSharedSecret(const uint8_t *otherPubTokenBytes, 
    size_t otherPubTokenBytesLength, uint8_t *sharedSecretOutput, size_t sharedSecretOutputLength, 
    size_t *sharedSecretLength) {
  int ret;
  if ((ret = mbedtls_dhm_read_public(&m_dhm, otherPubTokenBytes, otherPubTokenBytesLength)) != 0) {
    LOG("Failed to load other public key.");
    return 1;
  }
  else
    LOG("Succeeded in reading other public key.");

  if ((ret = mbedtls_dhm_calc_secret(&m_dhm, sharedSecretOutput, sharedSecretOutputLength, sharedSecretLength,
      mbedtls_ctr_drbg_random, &m_ctr_drbg) != 0)) {
    LOG("Failed to derive shared secret.");
    return 1;
  }
  else
    LOG("Bytes of derived shared secret: " << endl << 
      PrintByteArray(sharedSecretOutput, 0, *sharedSecretLength) << endl);

  return 0;
}