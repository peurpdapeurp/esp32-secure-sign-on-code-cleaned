
// #include "security-helper.hpp"
// #include "../consts.hpp"
// #include <esp8266ndn.h>
// #include "../logger.hpp"
// #include "../helpers/print-byte-array-helper.hpp"

// #include <uECC.h>
// #include <AES.h>

// #define LOG(...) LOGGER(SecurityHelper, __VA_ARGS__)

// SecurityHelper::SecurityHelper()
// {
//   int ret;
//   if ((ret = mbedtls_ctr_drbg_seed( &m_ctr_drbg, mbedtls_entropy_func, &m_entropy,
//         NULL,
//         0)) != 0 )
//     LOG("Failed to seed the deterministic random number generator.");
//   else
//     LOG("Succeeded in seeding deterministic random number generator.");

//   if ((ret = mbedtls_dhm_parse_dhm(&m_dhm, DH_PARAMS_PEM, sizeof(DH_PARAMS_PEM)) != 0))
//     LOG("Failed to read DH parameters.");
//   else
//     LOG("Succeeded in reading DH parameters.");

//   mbedtls_pk_init(&m_pk);
//   int signResult = mbedtls_pk_parse_key(&m_pk, RSA_KEY_PAIR_PEM_FILE, sizeof(RSA_KEY_PAIR_PEM_FILE), nullptr, 0);
//   if (signResult != 0)
//     LOG("Parsing RSA private key file failed: " << signResult << endl);
// }

// SecurityHelper::~SecurityHelper()
// {
//   mbedtls_entropy_free(&m_entropy);
//   mbedtls_ctr_drbg_free(&m_ctr_drbg);
//   mbedtls_dhm_free(&m_dhm);
// }

// bool
// SecurityHelper::verifyDataBySymmetricKey(const uint8_t *keyBytes, size_t keyLength, const ndn::DataLite &data)
// {
//   ndn_NameComponent dataNameComps[MAX_NAME_COMPS];
//   ndn_NameComponent dataKeyLocatorNameComps[MAX_NAME_COMPS];
//   ndn::DataLite tempData(dataNameComps, MAX_NAME_COMPS, dataKeyLocatorNameComps, MAX_NAME_COMPS);

//   tempData.set(data);

//   ndn::HmacKey key(keyBytes, keyLength);

//   uint8_t encoding[3000];
//   ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
//   size_t encodingLength, signedPortionBeginOffset, signedPortionEndOffset;
//   ndn_Error error;
//   if ((error = ndn::Tlv0_2WireFormatLite::encodeData
//       (tempData, &signedPortionBeginOffset,
//       &signedPortionEndOffset, output, &encodingLength))) {
//     LOG("Error encoding data from bootstrapping response, in order to do verification by TSK.");
//     LOG("Error: " << ndn_getErrorString(error) << endl);
//     return false;
//   }

//     if (key.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
//         tempData.getSignature().getSignature().buf(), tempData.getSignature().getSignature().size())) {
//       LOG("Successfully verified bootstrapping request response data packet.");
//     }
//     else {
//       LOG("Bootstrapping request response data packet failed to verify.");
//       return false;
//     }

//     return true;
// }

// void 
// SecurityHelper::decrypt(const uint8_t * cipherText, size_t cipherTextLength, uint8_t *key, 
//   const size_t keyLength, uint8_t *outputBuffer) {

//   AES aes;

//   uint8_t cipherTextCopy[cipherTextLength];
//   memcpy(cipherTextCopy, cipherText, cipherTextLength);

//   LOG("Cipher text copy: " << endl << PrintByteArray(cipherTextCopy, 0, cipherTextLength));

//   aes.iv_inc();
  
//   byte iv[N_BLOCK];
//   unsigned long long myIv = 0;
//   for (int i = 0; i < N_BLOCK; i++) {
//     iv[i] = 0;
//   }
 
//   aes.set_IV(myIv);
//   aes.get_IV(iv);
  
//   aes.do_aes_decrypt(cipherTextCopy, cipherTextLength, outputBuffer, key, keyLength*8, iv);
  
// }

// int 
// SecurityHelper::RNG(uint8_t *dest, unsigned size) {
//   // Use the least-significant bits from the ADC for an unconnected pin (or connected to a source of 
//   // random noise). This can take a long time to generate random data if the result of analogRead(0) 
//   // doesn't change very frequently.
//   while (size) {
//     uint8_t val = 0;
//     for (unsigned i = 0; i < 8; ++i) {
//       int init = esp_random();
//       int count = 0;
//       while (esp_random() == init) {
//         ++count;
//       }
      
//       if (count == 0) {
//          val = (val << 1) | (init & 0x01);
//       } else {
//          val = (val << 1) | (count & 0x01);
//       }
//     }
//     *dest = val;
//     ++dest;
//     --size;
//   }
//   // NOTE: it would be a good idea to hash the resulting random data using SHA-256 or similar.
//   return 1;
// }

// bool
// SecurityHelper::checkBuffersEqual(const uint8_t *buf1, size_t buf1Length, const uint8_t *buf2, size_t buf2Length) {

//   if (buf1Length != buf2Length)
//     return false;

//   for (int i = 0; i < buf1Length; i++) {
//     if (buf1[i] != buf2[i])
//       return false;
//   }

//   return true;
// }

// void
// SecurityHelper::generateApplicationLevelInterestAsymmetricSignatureECC(const ndn::InterestLite& interest,  
//   const uint8_t *key, size_t keyLength, uint8_t *signatureOutput, size_t *signatureLength,
//   uECC_Curve_t *curve) {

//   // will just do interest signing without a signature info for now

//   int privateKeySize = uECC_curve_private_key_size(curve);

//   uint8_t encoding[3000];
//   ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
//   size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
//   ndn_Error error;
//   if ((error = ndn::Tlv0_2WireFormatLite::encodeName
//       (interest.getName(), &signedPortionBeginOffset, &signedPortionEndOffset, output, &encodingLength))) {
//       LOG("Error decoding interest name for signing.");
//       LOG("Error: " << ndn_getErrorString(error) << endl);
//       return;
//   }

//   uint8_t interestNameDigest[ndn_SHA256_DIGEST_SIZE];
//   ndn_digestSha256(encoding, encodingLength, interestNameDigest);

//   *signatureLength = (privateKeySize * 2);
//   uECC_sign(key, interestNameDigest, ndn_SHA256_DIGEST_SIZE, signatureOutput + 8, curve);

//   LOG("Bytes of signed portion hash: " << endl << PrintByteArray(interestNameDigest, 0, ndn_SHA256_DIGEST_SIZE) << endl);

//   LOG("Bytes of signature generated for interest: " << endl << PrintByteArray(signatureOutput + 8, 0, *signatureLength) << endl);

// }

// void
// SecurityHelper::generateEcKeyPair(uint8_t *pub, uint8_t* pri, uECC_Curve_t *curve)
// {
//   uECC_set_rng(&RNG);
//   uECC_make_key(pub, pri, curve);
// }

// void
// SecurityHelper::deriveSharedSecretECDH(const uint8_t *pub, const uint8_t *pri, uint8_t *sharedSecretBytes,
//   uECC_Curve_t *curve)
// {
//   uECC_set_rng(&RNG);
//   uECC_shared_secret(pub, pri, sharedSecretBytes, curve);
// }

// void
// SecurityHelper::generateDHKeyPair(uint8_t *tokenOutput, size_t *tokenSize, int privateExponentNumBytes) {
//   int ret;
//   if ((ret = mbedtls_dhm_make_public(&m_dhm, privateExponentNumBytes, tokenOutput, m_dhm.len, 
//         mbedtls_ctr_drbg_random, &m_ctr_drbg)))
//     LOG("Failed to make public token.");
//   else
//     LOG("Bytes of public token: " << endl << PrintByteArray(tokenOutput, 0, m_dhm.len) << endl);

//   *tokenSize = m_dhm.len;
// }

// void
// SecurityHelper::readDHPublicAndDeriveSharedSecret(const uint8_t *otherPubTokenBytes, size_t otherPubTokenBytesLength,
//     uint8_t *sharedSecretOutput, size_t *sharedSecretLength) {
//   int ret;
//   if ((ret = mbedtls_dhm_read_public(&m_dhm, otherPubTokenBytes, otherPubTokenBytesLength)) != 0)
//     LOG("Failed to load other public key.");
//   else
//     LOG("Succeeded in reading other public key.");

//   if ((ret = mbedtls_dhm_calc_secret(&m_dhm, sharedSecretOutput, DH_ARRAY_SIZES, sharedSecretLength, 
//         mbedtls_ctr_drbg_random, &m_ctr_drbg) != 0))
//     LOG("Failed to derive shared secret.");
//   else
//     LOG("Bytes of derived shared secret: " << endl << PrintByteArray(sharedSecretOutput, 0, *sharedSecretLength) << endl);
// }

// void
// SecurityHelper::generateApplicationLevelInterestAsymmetricSignatureRSA(const ndn::InterestLite &interest, uint8_t *signatureOutput,
//     size_t *signatureLength) {

//   uint8_t encoding[3000];
//   ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
//   size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
//   ndn_Error error;
//   if ((error = ndn::Tlv0_2WireFormatLite::encodeName
//       (interest.getName(), &signedPortionBeginOffset, &signedPortionEndOffset, output, &encodingLength))) {
//       LOG("Error decoding interest name for signing.");
//       LOG("Error: " << ndn_getErrorString(error) << endl);
//       return;
//   }

//   uint8_t interestNameDigest[ndn_SHA256_DIGEST_SIZE];
//   ndn_digestSha256(encoding, encodingLength, interestNameDigest);

//   LOG("Digest of what we are signing with RSA: " << endl << 
//   PrintByteArray(interestNameDigest, 0, ndn_SHA256_DIGEST_SIZE) << endl);

//   int ret;
//   if ((ret = mbedtls_pk_sign(&m_pk, MBEDTLS_MD_SHA256, interestNameDigest, ndn_SHA256_DIGEST_SIZE, 
//     signatureOutput, signatureLength, NULL, NULL)) != 0)
//     LOG("Generation of application level interest signature with RSA failed.");

// }