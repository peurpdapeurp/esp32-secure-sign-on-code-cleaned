// #ifndef SECURITY_HELPER_HPP
// #define SECURITY_HELPER_HPP

// #include <stdint.h>
// #include <stddef.h>

// #include "mbedtls/config.h"
// #include "mbedtls/platform.h"
// #include "mbedtls/entropy.h"  // entropy gathering
// #include "mbedtls/ctr_drbg.h" // CSPRNG
// #include "mbedtls/error.h"    // error code to string conversion
// #include "mbedtls/md.h"       // hash algorithms
// #include "mbedtls/sha256.h"
// #include "mbedtls/dhm.h"
// #include "mbedtls/pk.h"

// #include <Streaming.h>

// #include <esp8266ndn.h>

// #include <AES.h>

// #include "../consts.hpp"

// /** \brief Parser to interpret the response to the bootstrapping request.
//  */
// class SecurityHelper
// {
// public:
//   explicit SecurityHelper();

//   ~SecurityHelper();

//   static bool
//   verifyDataBySymmetricKey(const uint8_t *keyBytes, size_t keyLength, const ndn::DataLite &data);

//   static void
//   decrypt(const uint8_t *cipherText, size_t cipherTextLength, uint8_t *key, const size_t keyLength, uint8_t *outputBuffer);

//   static bool
//   checkBuffersEqual(const uint8_t *buf1, size_t buf1Length, const uint8_t *buf2, size_t buf2Length);

//   static void
//   generateApplicationLevelInterestAsymmetricSignatureECC(const ndn::InterestLite &interest, 
//     const uint8_t *key, size_t keyLength, uint8_t *signatureOutput, 
//     size_t *signatureLength, uECC_Curve_t *curve);

//   static void
//   generateEcKeyPair(uint8_t *pub, uint8_t *pri, uECC_Curve_t *curve);

//   static void
//   deriveSharedSecretECDH(const uint8_t *EKpub, const uint8_t *DKpri, uint8_t *sharedSecretBytes, uECC_Curve_t *curve);

//   static void
//   generateApplicationLevelInterestAsymmetricSignatureRSA(const ndn::InterestLite &interest, uint8_t *signatureOutput,
//   size_t *signatureLength);

//   static void
//   generateDHKeyPair(uint8_t *tokenOutput, size_t *tokenSize, int privateExponentNumBytes);

//   static void
//   readDHPublicAndDeriveSharedSecret(const uint8_t *otherPubTokenBytes, size_t otherPubTokenBytesLength,
//     uint8_t *sharedSecretOutput, size_t *sharedSecretLength);

//   /** \brief Determine ASN1 length of integer at integer[0:20].
//  */
//   static inline int
//   determineAsn1IntLength(const uint8_t *integer)
//   {
//     // if the top bit of the first integer is 1, then an extra 0 byte needs to be added 
//     // to keep the integer positive since it is encoded in two's complement
//     if ((integer[0] & 0x80) != 0x00)
//     {
//       return m_Ks_curve_pri_key_size + 1;
//     }

//     // if the top bit of the first integer is 0, then still need to check if there are more
//     // zero bytes that can be cut off, since the ASN.1 encoding of a r and s ECDSA signature pair
//     // requires that r and s be represented a minimal amount of bytes possible
//     int len = m_Ks_curve_pri_key_size;
//     for (int i = 0; i < m_Ks_curve_pri_key_size - 1; ++i)
//     {
//       if (((integer[i] << 8) | (integer[i + 1] & 0x80)) != 0x0000)
//       {
//         break;
//       }
//       --len;
//     }
//     return len;
//   }

//   static inline uint8_t *
//   writeAsn1Int(uint8_t *output, const uint8_t *integer, int length)
//   {
//     *(output++) = ASN1_INTEGER;
//     *(output++) = static_cast<uint8_t>(length);

//     if (length == m_Ks_curve_pri_key_size + 1)
//     {
//       *(output++) = 0x00;
//       memmove(output, integer, m_Ks_curve_pri_key_size);
//       return output + m_Ks_curve_pri_key_size;
//     }

//     memmove(output, integer + m_Ks_curve_pri_key_size - length, length);
//     return output + length;
//   }

//   /** \brief Encode 40-octet raw signature at sig[8:48] as DER at sig[0:retval].
//  */
//   static inline int
//   encodeSignatureBits(uint8_t *sig)
//   {
//     const uint8_t *begin = sig;
//     const uint8_t *r = sig + 8;
//     const uint8_t *s = r + m_Ks_curve_pri_key_size;
//     int rLength = determineAsn1IntLength(r);
//     int sLength = determineAsn1IntLength(s);

//     *(sig++) = ASN1_SEQUENCE;
//     *(sig++) = 2 + rLength + 2 + sLength;
//     sig = writeAsn1Int(sig, r, rLength);
//     sig = writeAsn1Int(sig, s, sLength);

//     return sig - begin;
//   }

//   static int
//   RNG(uint8_t *dest, unsigned size);

//   mbedtls_entropy_context m_entropy;
//   mbedtls_ctr_drbg_context m_ctr_drbg;
//   mbedtls_md_context_t m_ctx;

//   mbedtls_dhm_context m_dhm;
//   mbedtls_pk_context m_pk;

//   //const struct uECC_Curve_t *m_ecdh_curve;
//   const struct uECC_Curve_t *m_Ks_curve;

//   size_t m_Ks_curve_pri_key_size;

//   //AES m_aes;

//   enum
//   {
//     ASN1_SEQUENCE = 0x30,
//     ASN1_INTEGER = 0x02,
//   };
// };

// #endif // SECURITY_HELPER_HPP