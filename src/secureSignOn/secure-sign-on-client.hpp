#ifndef SECURE_SIGN_ON_CLIENT_HPP
#define SECURE_SIGN_ON_CLIENT_HPP

#include <esp8266ndn.h>
#include <PriUint64.h>
#include "secure-sign-on-client-consts.hpp"

/** \brief Generic client for secure sign-on protocol.
 */
class SecureSignOnClient : public ndn::PacketHandler
{
public:

  SecureSignOnClient(ndn::Face &face, const uint8_t *deviceIdentifier, size_t deviceIdentifierLength,
    const uint8_t *deviceCapabilities, size_t deviceCapabilitiesLength,
    const uint8_t *secureSignOnCode, size_t secureSignOnCodeLength);

  ~SecureSignOnClient() noexcept;

  bool
  onboardingCompleted();

  const ndn::NameLite&
  getNetworkPrefix();

  bool
  begin();

protected:

  virtual bool 
  processData(const ndn::DataLite& data, uint64_t endpointId) override;

protected:

  ndn::Face& m_face;

  size_t m_deviceIdentifierLength;
  uint8_t m_deviceIdentifier[SECURE_SIGN_ON_CLIENT_DEVICE_IDENTIFIER_MAX_LENGTH];
  size_t m_deviceCapabilitiesLength;
  uint8_t m_deviceCapabilities[SECURE_SIGN_ON_CLIENT_DEVICE_CAPABILITIES_MAX_LENGTH];
  size_t m_secureSignOnCodeLength;
  uint8_t m_secureSignOnCode[SECURE_SIGN_ON_CLIENT_SECURE_SIGN_ON_CODE_MAX_LENGTH];

  char m_bootstrapPrefixString[13] = "/ndn/sign-on";
  ndn_NameComponent m_bootstrapPrefixComps[2];
  ndn::NameLite m_bootstrapPrefix;

  char m_certNameComponentString[6] = "/cert";
  ndn_NameComponent m_certNameComponentComps[1];
  ndn::NameLite m_certNameComponent;

  ndn_NameComponent m_networkPrefixComps[MAX_NAME_COMPS];
  ndn::NameLite m_networkPrefix;

  uint8_t m_trustAnchor[SECURE_SIGN_ON_CLIENT_TRUST_ANCHOR_MAX_LENGTH];
  size_t m_trustAnchorLength;

  uint8_t m_KdPubCertificateBytes[SECURE_SIGN_ON_CLIENT_KD_PUB_CERTIFICATE_MAX_LENGTH];
  size_t m_KdPubCertificateBytesLength;

  uint8_t m_KdPriBytes[SECURE_SIGN_ON_CLIENT_KD_PRI_MAX_LENGTH];
  size_t m_KdPriLength;

  bool m_onboarding_completed;

  std::unique_ptr<ndn::PacketBuffer> m_lastBootstrapResponse;

};

#endif // SECURE_SIGN_ON_CLIENT_HPP
