
#include "secure-sign-on-client.hpp"
#include "../utils/logger.hpp"

#define LOG(...) LOGGER(SecureSignOnClient, __VA_ARGS__)

SecureSignOnClient::SecureSignOnClient(ndn::Face &face, const uint8_t *deviceIdentifier, 
  size_t deviceIdentifierLength, const uint8_t *deviceCapabilities, 
  size_t deviceCapabilitiesLength, const uint8_t *secureSignOnCode, size_t secureSignOnCodeLength)
    : m_face(face),
      m_bootstrapPrefix(m_bootstrapPrefixComps, 2),
      m_certNameComponent(m_certNameComponentComps, 1),
      m_networkPrefix(m_networkPrefixComps, MAX_NAME_COMPS)
{

  ndn::parseNameFromUri(m_bootstrapPrefix, m_bootstrapPrefixString);
  ndn::parseNameFromUri(m_certNameComponent, m_certNameComponentString);

  m_deviceIdentifierLength = deviceIdentifierLength;
  memcpy(m_deviceIdentifier, deviceIdentifier, deviceIdentifierLength);
  m_deviceCapabilitiesLength = deviceCapabilitiesLength;
  memcpy(m_deviceCapabilities, deviceCapabilities, deviceCapabilitiesLength);
  m_secureSignOnCodeLength = secureSignOnCodeLength;
  memcpy(m_secureSignOnCode, secureSignOnCode, secureSignOnCodeLength);

  m_onboarding_completed = false;
}

SecureSignOnClient::~SecureSignOnClient()
{
  m_face.removeHandler(this);
};

bool 
SecureSignOnClient::onboardingCompleted()
{
  return m_onboarding_completed;
}

const ndn::NameLite &
SecureSignOnClient::getNetworkPrefix()
{
  return m_networkPrefix;
}

bool 
SecureSignOnClient::begin()
{
  LOG("Checking that bootstrap prefix was parsed correctly: "
      << ndn::PrintUri(m_bootstrapPrefix) << endl);
  LOG("Checking that certificate request name component was parsed correctly: "
      << ndn::PrintUri(m_certNameComponent) << endl);

  m_face.addHandler(this);
  return true;
};

bool 
SecureSignOnClient::processData(const ndn::DataLite &data, uint64_t endpointId)
{
    
}
