#include "sign-on-basic-ecc-hybrid.hpp"

#include "../../../../../utils/logger.hpp"
#include "../../../../../utils/print-byte-array-helper.hpp"
#include "../../../../../utils/security-helpers/general-security-helper.hpp"

#include <uECC.h>

#define LOG(...) LOGGER(SignOnBasicECCHybrid, __VA_ARGS__)

SignOnBasicECCHybrid::SignOnBasicECCHybrid(ndn::Face &face, const uint8_t *device_identifier,
  const uint8_t *device_capabilities, const uint8_t *secure_sign_on_code, const uint8_t *BKpub,
  const uint8_t *BKpri)
    : SignOnBasicClient(face, device_identifier, device_capabilities, secure_sign_on_code,
      SIGN_ON_BASIC_ECC_HYBRID_SECURE_SIGN_ON_CODE_LENGTH)
{
  m_BKpub = BKpub;
  m_BKpri = BKpri;
}

int 
SignOnBasicECCHybrid::generateDiffieHellmanKeyPair(uint8_t *N1Pub, size_t *N1Pub_length,
  uint8_t *N1Pri, size_t *N1Pri_length) 
{
  return m_ecdh_helper.generateEcKeyPair(N1Pub, N1Pub_length, N1Pri, N1Pri_length, 
    SIGN_ON_BASIC_ECC_HYBRID_ECDH_AND_KD_CURVE);
}

int 
SignOnBasicECCHybrid::generateApplicationLevelInterestAsymmetricSignature(ndn::InterestLite &interest,
  uint8_t *signatureBuffer, size_t *signatureLength)
{
  return m_ecdsa_helper.generateApplicationLevelInterestAsymmetricSignatureECC(interest, 
    m_BKpub, sizeof(m_BKpub), signatureBuffer, signatureLength, SIGN_ON_BASIC_ECC_HYBRID_KS_CURVE);
}

int
SignOnBasicECCHybrid::deriveDiffieHellmanSharedSecret(uint8_t *sharedSecretBytes, 
  size_t *sharedSecretLength)
{
  *sharedSecretLength = uECC_curve_private_key_size(uECC_secp160r1());
  return m_ecdh_helper.deriveSharedSecretECDH(m_N2Pub, m_N1Pri, m_sharedSecret, 
    SIGN_ON_BASIC_ECC_HYBRID_ECDH_AND_KD_CURVE);
}

bool
SignOnBasicECCHybrid::verifyBootstrappingResponse(const ndn::DataLite &data, const uint8_t *key)
{
  return GeneralSecurityHelper::verifyDataBySymmetricKey(data, key, 
    SIGN_ON_BASIC_ECC_HYBRID_SECURE_SIGN_ON_CODE_LENGTH);
}

void
SignOnBasicECCHybrid::decryptKdPri(const uint8_t *encryptedKdPri, size_t encryptedKdPriLength, uint8_t *Kt, 
    const size_t KtLength, uint8_t *outputBuffer, size_t *outputLength) 
{
  *outputLength = uECC_curve_private_key_size(SIGN_ON_BASIC_ECC_HYBRID_ECDH_AND_KD_CURVE);
  GeneralSecurityHelper::decrypt(encryptedKdPri, encryptedKdPriLength, Kt, KtLength, outputBuffer);
}