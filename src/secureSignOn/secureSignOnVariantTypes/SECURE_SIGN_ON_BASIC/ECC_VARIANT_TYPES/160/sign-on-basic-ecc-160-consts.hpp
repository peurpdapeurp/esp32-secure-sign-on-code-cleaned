#ifndef SIGN_ON_BASIC_ECC_160_CONSTS_HPP
#define SIGN_ON_BASIC_ECC_160_CONSTS_HPP

#include <uECC.h>

constexpr int SIGN_ON_BASIC_ECC_160_SECURE_SIGN_ON_CODE_LENGTH = 10;

// curve used for the diffie hellman exchange, and for the keypair generated on the
// controller side which we will get a certificate and the private key of;
// the current ecc sign on basic variants all use the same curve for both ecdh and the
// generation of the Kd keypair
const uECC_Curve SIGN_ON_BASIC_ECC_160_ECDH_AND_KD_CURVE = uECC_secp160r1();
// curve used for the bootstrapping keypair
const uECC_Curve SIGN_ON_BASIC_ECC_160_KS_CURVE = uECC_secp160r1();

#endif // SIGN_ON_BASIC_ECC_160_CONSTS_HPP