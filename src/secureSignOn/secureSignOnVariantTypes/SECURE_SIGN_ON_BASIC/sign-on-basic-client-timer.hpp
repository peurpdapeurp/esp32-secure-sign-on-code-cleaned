#ifndef SING_ON_BASIC_CLIENT_TIMER_HPP
#define SING_ON_BASIC_CLIENT_TIMER_HPP

#include <stdint.h>
#include <Streaming.h>
#include <PriUint64.h>

class SignOnBasicClientTimer {
public:
  // this time is set as soon as the device calls the sendBootstrappingRequest function
  uint64_t m_lastOnboardingStartTime;
  // this time is set as soon as the device sends the bootstrapping request
  uint64_t m_lastOnboardingSentBootstrapRequestTime;
  // this time is set as soon as the device receives the bootstrapping response
  uint64_t m_lastOnboardingReceivedBootstrapResponseTime;
  // this time is set as soon as the device finishes processing the bootstrapping response
  uint64_t m_lastOnboardingFinishedProcessingBootstrappingResponseTime;
  // this time is set as soon as the device sends the certificate request
  uint64_t m_lastOnboardingSentCertificateRequestTime;
  // this time is set as soon as the device receives the certificate request response
  uint64_t m_lastOnboardingReceivedCertificateRequestResponseTime;
  // this time is set as soon as the device finishes processing the certificate request response
  uint64_t m_lastOnboardingFinishTime;

  void
  printLastExecutionTimes();

};

#endif // SING_ON_BASIC_CLIENT_TIMER_HPP