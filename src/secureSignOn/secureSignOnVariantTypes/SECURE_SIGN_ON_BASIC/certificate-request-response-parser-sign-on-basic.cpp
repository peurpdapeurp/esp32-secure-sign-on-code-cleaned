
#include "certificate-request-response-parser-sign-on-basic.hpp"
#include <esp8266ndn.h>
#include "../../../utils/tlv-helpers.hpp"
#include "../../../utils/logger.hpp"
#include "../../secureSignOnTLVs/secure-sign-on-tlvs.hpp"
#include "../../../utils/ndn-tlv-types.hpp"

#define LOG(...) LOGGER(CertificateRequestResponseParserSignOnBasic, __VA_ARGS__)

CertificateRequestResponseParserSignOnBasic::CertificateRequestResponseParserSignOnBasic(const uint8_t* payload, 
  size_t payloadLen)
  : m_keyNameComponent(m_keyNameComp, 1)
{
  m_payload = payload;
  m_payloadLen = payloadLen;

  ndn::parseNameFromUri(m_keyNameComponent, m_keyNameComponentString);
}

CertificateRequestResponseParserSignOnBasic::~CertificateRequestResponseParserSignOnBasic()
{

}

bool
CertificateRequestResponseParserSignOnBasic::processCertificateRequestResponse(uint8_t *KdPubCertificateBytes, 
  size_t *KdPubCertificateBytesLength, uint8_t *KdPriEncrypted, size_t *KdPriEncryptedLength)
{
  LOG("Process certificate request response got called." << endl);

  ndn_TlvDecoder_initialize(&m_received_certificate_decoder, m_payload, m_payloadLen);

  if (!(readTypeAndLength(m_received_certificate_decoder, KD_PRI_ENCRYPTED_TLV_TYPE,
    m_receivedKdPriEncryptedTlvTypeAndLengthSize, m_receivedKdPriEncryptedTlvValueSize, "KdPri encrypted") &&
    readTypeAndLength(m_received_certificate_decoder, DATA_TLV_TYPE,
    m_receivedCertificateTlvTypeAndLengthSize, m_receivedCertificateTlvValueSize, "received certificate")))
    return false;

  *KdPriEncryptedLength = m_receivedKdPriEncryptedTlvValueSize;
  LOG("KdPriEncryptedLength: " << *KdPriEncryptedLength);
  memcpy(KdPriEncrypted, m_payload + m_receivedKdPriEncryptedTlvTypeAndLengthSize, 
    *KdPriEncryptedLength);

  *KdPubCertificateBytesLength = 
    m_receivedCertificateTlvTypeAndLengthSize + m_receivedCertificateTlvValueSize;
  LOG("KdPubCertificateBytesLength: " << *KdPubCertificateBytesLength);
  memcpy(KdPubCertificateBytes, m_payload + m_receivedKdPriEncryptedTlvTypeAndLengthSize
    + m_receivedKdPriEncryptedTlvValueSize, *KdPubCertificateBytesLength);

}