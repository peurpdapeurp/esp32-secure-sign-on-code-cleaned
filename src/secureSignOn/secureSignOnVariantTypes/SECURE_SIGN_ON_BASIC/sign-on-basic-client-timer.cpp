
#include "sign-on-basic-client-timer.hpp"

void 
SignOnBasicClientTimer::printLastExecutionTimes()
{
    uint64_t T1 = m_lastOnboardingSentBootstrapRequestTime - m_lastOnboardingStartTime;
    uint64_t T2 = m_lastOnboardingReceivedBootstrapResponseTime - m_lastOnboardingSentBootstrapRequestTime;
    uint64_t T3 = m_lastOnboardingFinishedProcessingBootstrappingResponseTime - m_lastOnboardingReceivedBootstrapResponseTime;
    uint64_t T4 = m_lastOnboardingSentCertificateRequestTime - m_lastOnboardingFinishedProcessingBootstrappingResponseTime;
    uint64_t T5 = m_lastOnboardingReceivedCertificateRequestResponseTime - m_lastOnboardingSentCertificateRequestTime;
    uint64_t T6 = m_lastOnboardingFinishTime - m_lastOnboardingReceivedCertificateRequestResponseTime;
    uint64_t T7 = m_lastOnboardingFinishTime - m_lastOnboardingStartTime;
    uint64_t T8 = T1 + T3 + T4 + T6;

    Serial << "Timers from last onboarding:" << endl;
    Serial << "T1: " << PriUint64<DEC>(T1) << endl;
    Serial << "T2: " << PriUint64<DEC>(T2) << endl;
    Serial << "T3: " << PriUint64<DEC>(T3) << endl;
    Serial << "T4: " << PriUint64<DEC>(T4) << endl;
    Serial << "T5: " << PriUint64<DEC>(T5) << endl;
    Serial << "T6: " << PriUint64<DEC>(T6) << endl;
    Serial << "T7: " << PriUint64<DEC>(T7) << endl;
    Serial << "T8: " << PriUint64<DEC>(T8) << endl;

    Serial << "CSV format: " << endl;
    Serial << T1 << "," << T2 << "," << T3 << "," << T4 << "," << T5 << "," << T6 << "," << T7 << "," << T8 << endl;
}