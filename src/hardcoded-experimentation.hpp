#ifndef HARDCODED_EXPERIMENTATION_HPP
#define HARDCODED_EXPERIMENTATION_HPP

#include "secureSignOn/secure-sign-on-client-consts.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/sign-on-basic-client-consts.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/ECC_VARIANT_TYPES/160/sign-on-basic-ecc-160-consts.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/ECC_VARIANT_TYPES/HYBRID/sign-on-basic-ecc-hybrid-consts.hpp"
#include "secureSignOn/secureSignOnVariantTypes/SECURE_SIGN_ON_BASIC/RSA_VARIANT_TYPES/1024/sign-on-basic-rsa-1024-consts.hpp"

#include <stdint.h>

#include "secure-sign-on-variant-selector.hpp"

const uint8_t DEVICE_IDENTIFIER[SIGN_ON_BASIC_CLIENT_DEVICE_IDENTIFIER_LENGTH] = {
  0xBB, 0x05, 0xFB, 0x4A, 0xCB, 0x4B, 0xC7, 0x2B, 0x9C, 0xDF,
  0xAE, 0xE4,
};

const uint8_t DEVICE_CAPABILITIES [SIGN_ON_BASIC_CLIENT_DEVICE_CAPABILITIES_LENGTH] = {
  0xFE, 
};

#if defined(SIGN_ON_BASIC_ECC_160)

// these are the raw xx bytes of the bootstrapping ecc private key
const uint8_t BOOTSTRAP_ECC_PRIVATE[21] {
  0x00, 0x78, 0xe5, 0xb3, 0x8a, 0x02, 0x84, 0x1a, 0xab, 0x69, 
  0x1c, 0x71, 0x0a, 0x0c, 0x48, 0x7c, 0x57, 0x8b, 0xcd, 0x79, 
  0x1f
};

// these are the ASN encoded bytes of the bootstrapping ecc public key;
// this means there are xx bytes prepended to the raw public key bytes which
// represent an ASN header, and the last xx bytes of this array are the
// actual raw public key bytes
const uint8_t BOOTSTRAP_ECC_PUBLIC[64] {
  0x30, 0x3e, 0x30, 0x10, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 
  0x3d, 0x02, 0x01, 0x06, 0x05, 0x2b, 0x81, 0x04, 0x00, 0x08, 
  0x03, 0x2a, 0x00, 0x04, 0xa2, 0xf2, 0x13, 0x87, 0xb8, 0xf0, 
  0x07, 0xdd, 0x3a, 0x0e, 0x85, 0x63, 0x0d, 0x7e, 0xb2, 0x86, 
  0xbf, 0xb8, 0x0e, 0x43, 0x7c, 0xcc, 0xe8, 0x50, 0x43, 0xd7, 
  0x54, 0x7d, 0xc2, 0x56, 0xe6, 0x4a, 0xc4, 0x18, 0x9b, 0xea, 
  0x28, 0x57, 0xa7, 0x70
};

const uint8_t SECURE_SIGN_ON_CODE[SIGN_ON_BASIC_ECC_160_SECURE_SIGN_ON_CODE_LENGTH] = {
  0x30, 0x30, 0x13, 0x06, 0x07, 0x2a, 0xce, 0x3d, 0x02, 0x06
};

#endif

#if defined(SIGN_ON_BASIC_ECC_HYBRID) || defined(SIGN_ON_BASIC_ECC_256)

// these are the raw 32 bytes of the bootstrapping ecc private key
const uint8_t BOOTSTRAP_ECC_PRIVATE[32] {
  0xD8, 0x9A, 0x9E, 0xD9, 0xD4, 0x5A, 0xFD, 0xA1, 0xE5, 0xA4, 
  0x29, 0x73, 0x2B, 0x18, 0xE5, 0x51, 0xC4, 0xB0, 0x77, 0xEF, 
  0xA3, 0x5E, 0xB3, 0x55, 0x63, 0x73, 0xBC, 0x13, 0xBE, 0xE2, 
  0x5C, 0x2C,
};

// these are the ASN encoded bytes of the bootstrapping ecc public key;
// this means there are 27 bytes prepended to the raw public key bytes which
// represent an ASN header, and the last 64 bytes of this array are the
// actual raw public key bytes
const uint8_t BOOTSTRAP_ECC_PUBLIC_ASN_ENCODED[91] {
  0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE, 
  0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 
  0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0x41, 0xA0, 0x02, 
  0x0C, 0x65, 0xCA, 0x1B, 0xD0, 0xB4, 0x4B, 0x0B, 0xC9, 0xD3, 
  0x92, 0xE2, 0x14, 0xDB, 0x7A, 0x97, 0xC3, 0x22, 0xEA, 0xC7, 
  0xD7, 0xEA, 0x05, 0x77, 0xFB, 0x74, 0x4C, 0xC0, 0x86, 0x8F, 
  0xA6, 0xF9, 0x21, 0x72, 0x38, 0x92, 0xF3, 0x69, 0xA9, 0xAA, 
  0x82, 0xE0, 0xEC, 0x69, 0x77, 0x59, 0xA8, 0x6C, 0x5E, 0x7D, 
  0x74, 0x96, 0x1D, 0xB9, 0xCD, 0x9A, 0x3D, 0xC0, 0x2F, 0x86, 
  0x4A,
};

// these are the raw key bytes of the ecc public key (including)
// the point identifier, which is the first byte
const uint8_t BOOTSTRAP_ECC_PUBLIC[65] {
  0x04, 
  0x41, 0xA0, 0x02, 0x0C, 0x65, 0xCA, 0x1B, 0xD0, 0xB4, 0x4B, 
  0x0B, 0xC9, 0xD3, 0x92, 0xE2, 0x14, 0xDB, 0x7A, 0x97, 0xC3, 
  0x22, 0xEA, 0xC7, 0xD7, 0xEA, 0x05, 0x77, 0xFB, 0x74, 0x4C, 
  0xC0, 0x86, 0x8F, 0xA6, 0xF9, 0x21, 0x72, 0x38, 0x92, 0xF3, 
  0x69, 0xA9, 0xAA, 0x82, 0xE0, 0xEC, 0x69, 0x77, 0x59, 0xA8, 
  0x6C, 0x5E, 0x7D, 0x74, 0x96, 0x1D, 0xB9, 0xCD, 0x9A, 0x3D, 
  0xC0, 0x2F, 0x86, 0x4A,
};

const uint8_t SECURE_SIGN_ON_CODE[SIGN_ON_BASIC_ECC_HYBRID_SECURE_SIGN_ON_CODE_LENGTH] = {
  0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE, 
  0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A,
};

#endif

#if defined(SIGN_ON_BASIC_RSA_1024)
// this is a 1024 RSA diffie hellman pem file
const unsigned char DH_PARAMS_PEM[] =
    "-----BEGIN DH PARAMETERS-----\n"
    "MIGHAoGBAP//////////yQ/aoiFowjTExmKLgNwc0SkCTgiKZ8x0Agu+pjsTmyJR\n"
    "Sgh5jjQE3e+VGbPNOkMbMCsKbfJfFDdP4TVtbVHCReSFtXZiXn7G9ExC6aY37WsL\n"
    "/1y29Aa37e44a/taiZ+lrp8kEXxLH+ZJKGZR7OZTgf//////////AgEC\n"
    "-----END DH PARAMETERS-----";
// RSA 1024 pem key file
const unsigned char RSA_KEY_PAIR_PEM_FILE[] =
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIICXQIBAAKBgQDUjoGa38NDDg7/XJ9Mb0mC7HFOFibtOsgcIY48f51qXU82rvV0\n"
    "t0Pvyzs0GOvhQoGcGM/jK5T4Knf22GELiI+nDR1IqSQE/IBxN1E9WssZfnG3nnm0\n"
    "mDa7Gps1vhAilHRKzzUGmIV4WNOVKLT39mfcE3y+95kjYwd0w8cKqV5KVwIDAQAB\n"
    "AoGBAIuVDZ1DxjlZmRWuoFmUV0TnujxaJbPxvwliK8Kk1x3LKsKxQDUmSDHHVxiO\n"
    "KRBjGTQFwzbeHtSdJuxuCcSGKDaLouA1APRD04fRzS8Sy8NvWZOBXWm5G63DnJyo\n"
    "L2X7YkYn2F6rqHzzg7Tk68ONtnUxKyUcBseTQcQCRb/CeuTxAkEA8/nYoRk8JrUV\n"
    "Qdd7aAJzspy7fnNWN94nUm6a7kVxKsPkhoOfDRewN1XM2TXiW+pykqqB0VSkunjw\n"
    "VqFqsbcbKQJBAN8IQScsyKbsJI3FjvWFYEt3URYFLCm+V75JKs4aI3HD8tlj0Rqw\n"
    "PUEUR50i9xFSBniUq+zKA6VavbiIOArEaX8CQQChaORXAHj5KshbfRIZ57Vfr62j\n"
    "pO6fm4vsT1yU0sBiCKPCEoZBhTCCC3OxKKMPKHaQbdnhAdzRuX1X8wbBeGCxAkBv\n"
    "SVIhNwJj7/6JUyNzNPsIjN/D6g7QQW7MvvuB3Z8D/C8n89t5wqi92V2mdbTsSo56\n"
    "Ck0DGQXasz1pX2b/UGQHAkBXc/XW6dSm/eb1XMnw++vKcSx5LFa7uJ4S3WPv5C4H\n"
    "1FGylDOXI3WzX4C+e58IlBM8GLFv2j2kXX0BAKwKZL6P\n"
    "-----END RSA PRIVATE KEY-----";

    const uint8_t SECURE_SIGN_ON_CODE[SIGN_ON_BASIC_RSA_1024_SECURE_SIGN_ON_CODE_LENGTH] = {
      0x30, 0x30, 0x13, 0x06, 0x07, 0x2a, 0xce, 0x3d, 0x02, 0x06
    };

#endif

#if defined(SIGN_ON_BASIC_RSA_HYBRID)
// this is a 1024 RSA diffie hellman pem file
const unsigned char DH_PARAMS_PEM[] =
    "-----BEGIN DH PARAMETERS-----\n"
    "MIGHAoGBAP//////////yQ/aoiFowjTExmKLgNwc0SkCTgiKZ8x0Agu+pjsTmyJR\n"
    "Sgh5jjQE3e+VGbPNOkMbMCsKbfJfFDdP4TVtbVHCReSFtXZiXn7G9ExC6aY37WsL\n"
    "/1y29Aa37e44a/taiZ+lrp8kEXxLH+ZJKGZR7OZTgf//////////AgEC\n"
    "-----END DH PARAMETERS-----";
// RSA 3072 pem key file
const unsigned char RSA_KEY_PAIR_PEM_FILE[] =
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIIG4wIBAAKCAYEAmKBziaRy+blvsrjL/FgEI9g/+md8+rvF3b5JRPsxLabh3RhH\n"
    "bt+JwV3ulzxyVAsOJWMbP2a/Y903gY0ZDXKdiOOnYCuwuexgN1evX5pgr5hXsEmO\n"
    "JsrwfiPuxTOAq8CSqmkOEELLpSRLfDIdjjzRFkAmVC88hJPTw+iUaNod4Vijf9Su\n"
    "xpg59tfZcvXW99+vKpTNks5Lb9iWs4wziTrrN46QNQM7Wf5nW8rGId4X3ZPY2L+Z\n"
    "KO+xQKzmrlkT5VFKoIDmNWwL3dHd/LHKy/HN3BtF18CXJOWZuCEsQHOy3y7Mdec9\n"
    "JMJzQxT1Mh/Tvc1ZUsun8HJ3uhsKol/GwHjUKuZDn2n91PFUtLK0wJ6wqr7Q9dys\n"
    "xxN/BXBrGkO28WkJbJitIzN/A081ypIJ8X9HyU7tL5B0wxXXv53m7C4+iGYg9XMB\n"
    "Xk1ovu9z0FJyiN6B/ludhHN97uGseAWqTf9zvXOvcrhJ6pdXEc8T4bB5BaAVYvxY\n"
    "/qzR7bK3pwyWv8gRAgMBAAECggGADMreBDphXKb7QrqUdlkWN77nVeCYrBdS9Uv+\n"
    "riZzr8TeB38CdbuMu0VZOxnY50xp6h5NaVn+Tqt6/IPMBsGFjLSwEbJ3xI2YXusd\n"
    "iuDQm8ckCmWP4ZZ+48O1Ppgbf8nLQTc+84P1t6i8HvPAYGyehcPosoOnCeGuDSyQ\n"
    "S92ERZ/9TDiSUZjuus7KrgUG+I2qC+Ke/GAIGJ7JutDUfPsQPf7X/WmA7wEQvrPX\n"
    "OX14+Jxsv3YHq1Ozwpz7VL+omD7g5kFCl0MMoAQggiJ6gvHNSCaQqCSwMac4Vv7Q\n"
    "q3fRUpiyU5bIwN3LIJ2XglkyuBlVnEYM3oJKyzRSKHCzYkrLVwo7B/gzbXUuonmE\n"
    "5WMDZuLzUWz0wyo4PrM8WOBGabHjBPwmIeFVm1FIBuZw9gGCPJDHQxfNIkVeQU5k\n"
    "Gee9vu195Wq5zR0FQb4a3T/Lb6tu0sQJmdcTiiVB3zmGjfMpiVB3iBBsZ42y7cs1\n"
    "oU+lL66FaISXg/MuwfHg1ISE10nFAoHBAMjOaYZaasU9zRAKHH53kZ65ggI8UZuD\n"
    "dSbglLawNMkZg0Jd9OJVcdFcN6rL/rnYWt4n7wjo51ueiYPkc9cSDZZuz7UzmL1t\n"
    "37ujYRTfBmDaVVufAIyE9nLKAFk+xtMWkS+Li0lL0jZJRGUgDavWpvJ8UxPlskFi\n"
    "f92REf5T4TET5yIhgBwyJa30NZ0RDrxC4x98FZD4ddqOIRya1VmCiZ6SjNj+4/jB\n"
    "OWx+EUDPxIr4WmWaW2tsBd0GI2C+ojbdUwKBwQDCk+vntd6oaYnCJu2qWJ9qvEfJ\n"
    "JnyzZs27j+3KVctuThnKe+QHZg/TdwWVFuqgUEJ4YjxlpHDXU8FCJBaDJG16ttT7\n"
    "uZ2Lz4L4ZnXiGBWT1n3qS41e6Mmu9p8h7Z9CtN2cHf9oriRbu3PVpdEAhZ3mS3TI\n"
    "nUfK/IjtKJbWdvkQJQcH0B6yVs49PTUAqJZj8I7gOy+APfYsPnpqx0JoUGJipIJx\n"
    "FkJ6waJCjfMVs/zjP/vojOl2mvHDL/fxm30kdIsCgcEAikHsUBADAYkanpeHpN+o\n"
    "iJOLWwSy35M0Xy8z1VEfFvMU9+X/DLLt6ctHnLZVzRmU/u6Z2xWY7fELF598zrDk\n"
    "YBYttsIuwQZcjDHiTt974cWSDcI9O4L0uHYypT/e30njbsvye+XTd4Mcr0/ReeYS\n"
    "DlThUorNH8204hXXQLf6DbN5aXuNXFx+qvXA0EMFVUPsYyopIyae5nsDe1Fz4exJ\n"
    "2r9mcKWWyxgm9HccY/ZpiZGTk+8iUKUQqGxogVzgDUx3AoHAbxKHsN61+sc8N1OH\n"
    "ih1K62ANTnsk5jhCKtaPdFgjZ5U3zcmJMtwFHr6rth690sPeHeBd/9ut+o06HeqB\n"
    "21zZDhr+W6/qilvrtKawll8POjRP7oYwbkROlQz9bB28MxUSCENrgDMgWCkah5Q/\n"
    "SZ/8sfDS+o4l5G7iLxdje4uww5GPgNtCGqXI11ZwQi76opQ4yzGd/QPh2gHUWc3j\n"
    "T67Launj+HAvhOmyGXH0mKgFLShiQa7muqjaZ4Sjcg1Dk6LfAoHAKGH1Mowf5zp9\n"
    "hGDv+mR9Ab6Up2gqGBz80z5ATgnR8S2u7LtaitVX5Uql0NIhZ4JzRiEJgpN6T92y\n"
    "jLuYGqjnHe0/jTPhKf7HbPFzgA9cDs4cfAxU+3Xd3zcNHWP6usYG5CXex0+IBcIs\n"
    "3NsFx4IDFeAxw+s2y0JlVw5svjXH0XSJXvdDIkR7KOi2LFfuNJKLsXQ3UzhIftIk\n"
    "qQ2comnYZhwVTidX9QwH9PbO790uW1HlxcqCYlwFQeyDa74KU/oB\n"
    "-----END RSA PRIVATE KEY-----";

    const uint8_t SECURE_SIGN_ON_CODE[SIGN_ON_BASIC_ECC_HYBRID_SECURE_SIGN_ON_CODE_LENGTH] = {
      0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE, 
      0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A,
    };

#endif

#endif // HARDCODED_EXPERIMENTATION_HPP