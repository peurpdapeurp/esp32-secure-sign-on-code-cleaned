#include "connectivity.hpp"

#include <WiFi.h>

#include "credentials.hpp"
#include "utils/logger.hpp"
#include "consts.hpp"

#define LOG(...) LOGGER(Connectivity, __VA_ARGS__)

constexpr const char *WIFI_SSID = "edward3";
constexpr const char *WIFI_PASS = "11111111";

#define IP_CLIENT IPAddress(192, 168, 137, 215)
#define IP_SERVER IPAddress(192, 168, 137, 159)
#define IP_SUBNET IPAddress(255, 255, 255, 0)
#define UDP_PORT 6363

ConnectivityClass::ConnectivityClass()
  : m_transport()
  , m_face(m_transport)
{
}

void
ConnectivityClass::begin()
{
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.config(IP_CLIENT, IP_SERVER, IP_SUBNET);
  WiFi.begin(WIFI_SSID, WIFI_PASS);

  int timeout = 15000;
  while (WiFi.status() != WL_CONNECTED) {
    if (--timeout < 0) {
      LOG("WiFi connection timeout, rebooting...");
      ESP.restart();
    }
    delay(1);
  }

  LOG("WiFi ready");

  m_transport.beginTunnel(IP_SERVER, UDP_PORT, UDP_PORT);

  static ndn::DigestKey key;
  m_face.setSigningKey(key);
}

IPAddress
ConnectivityClass::localIP()
{
  return WiFi.localIP();
}

void
ConnectivityClass::loop()
{
  if (WiFi.status() != WL_CONNECTED) {
    LOG("WiFi connection lost, rebooting...");
    delay(1000);
    ESP.restart();
  }

  m_face.loop();
}

ConnectivityClass Connectivity;
